# code monkied from https://pytorch.org/tutorials/beginner/basics/quickstart_tutorial.html

# Pytorch offers domain-specific libraries like TochText, TorchVision and TorchAudio

import torch
from torch import nn
from torch.utils.data import DataLoader

# module contains Dateset-objects for many real-world vision data
# like CIFAR, COCO. In this tut. FashionMNist dataset is used.
# Every Dataset includes two arguments: transform and target_transform
# to modify the samples and labels respectively.
from torchvision import datasets
from torchvision.transforms import ToTensor

# Download traingin data
training_data = datasets.FashionMNIST(
    root = "data",
    train = True,
    download = True,
    transform = ToTensor(),
)

# Download test data from open datasets
test_data = datasets.FashionMNIST(
    root = "data",
    train = False,
    download = True,
    transform = ToTensor(),
)

# Dataset is passed as an argument to DataLoader. Wraps an iterable over dataset
# and supports automatic batching, sampling, shuffling and multiprocess data loaging.
# Batch size defined to 64, each element will return a batch of 64 features and labels.

batch_size = 64

# create data loaders
train_dataloader = DataLoader(training_data, batch_size=batch_size)
test_dataloader =  DataLoader(test_data, batch_size=batch_size)

for x, y in test_dataloader:
    print(f"shape of X [N, C, H, W]: {x.shape}")
    print(f"shape of y: {y.shape}{y.dtype}")
    break

# Neural network defined inheriting from nn.Module. Layers of the network in __init__
# function. How data will pass specified in the forward function.

device = (
    "cuda"
    if torch.cuda.is_available()
    else "mps"
    if torch.backends.mps.is_available()
    else "cpu"
)
print (f"Using {device} device")

# Define model
class NeuralNetwork(nn.Module):
    def __init__(self):
        super().__init__()
        self.flatten = nn.Flatten()
        self.linear_relu_stack = nn.Sequential(
            nn.Linear(28*28, 512),
            nn.ReLU(),
            nn.Linear(512, 512),
            nn.ReLU(),
            nn.Linear(512, 10)
        )
    def forward(self, x):
        x = self.flatten(x)
        logits = self.linear_relu_stack(x)
        return logits

model = NeuralNetwork().to(device)
print(model)

# To train a model, loss funtion and optimizer needed.
loss_fn = nn.CrossEntropyLoss()
optimizer = torch.optim.SGD(model.parameters(), lr=1e-3)

# In single training loop model makes predictions on the training dataset
# and backpropagates the prediction error to adjust the model's parameters.

def train(dataloader, model, loss_fn, optimizer):
    size = len(dataloader.dataset)
    model.train()
    for batch, (X,y) in enumerate(dataloader):
        X, y = X.to(device), y.to(device)

        # compute prediction error
        pred = model(X)
        loss = loss_fn(pred, y)

        # backpropagation
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        if batch % 100 == 0:
            loss, current = loss.item(), (batch + 1) * len(X)
            print(f"loss: {loss:>7f}  [{current:>5d}/{size:>5d}]")

# check model's performance against the test dataset to ensure it is learning.

def test(dataloader, model, loss_fn):
    size = len(dataloader.dataset)
    num_batches = len(dataloader)
    model.eval()
    test_loss, correct = 0, 0
    with torch.no_grad():
        for X, y in dataloader:
            X, y = X.to(device), y.to(device)
            pred = model(X)
            test_loss += loss_fn(pred, y).item()
            correct += (pred.argmax(1) == y).type(torch.float).sum().item()
    test_loss /= num_batches
    correct /= size
    print(f"Test error: \n Accuracy: {(100*correct):>0.1f}%, average loss: {test_loss:>8f} \n")

# training process is conducted over several iteration (epochs). During each epoch
# the model learns parameters to make better predictions.
# (accuracy increase, loss decrease with every epoch)

epochs = 5
for t in range(epochs):
    print(f"epoch {t+1}\n---------------------------------------")
    train(train_dataloader, model, loss_fn, optimizer)
    test(test_dataloader, model, loss_fn)
print("done didldy do")
print(
'''
                                                                          
                                     C                                    
            S                 Gs     (S                                   
           R@                  %GQ@KQ7(R#%                                
          S@@@                (R(ROOQOt6Os#R                              
           @@                 #O6(#sRR((#SOGC%                            
     R     @S                  #S(t(t((t(OCsOe                            
    /@@   O@      t             Stt(((((t(SOsO^                           
    @@@K  @K     @@             ~((((3(tt(#GOOG                           
    ~@t   @@     @BO           K   @K  K((#GsOR                           
     #%   @@    /@@t           %  %   ~ KKROeG#                           
    O@    @@     @@             %ttt6/S7t(SOe@%                           
    @B    @@     @@           ^RCOOG#3((t(GO#sO                           
   @#    ^@K      @K         OSeCOOQCQCK3ttsOOO//(/                       
   @s     @@      (@            /O3GR##K((tKO#RRRRB(                      
   %@R    #@      B#          @RRRRttt(ttttRRRBR#K#                       
    S@@    @R   R@@            6BRRGt((t(t(BK###KK                        
     /@@O (@@@@@@t              @BRs7((ttt7#####R                         
       ~%B@@@#                   @B(tt(t(BK####C                          
           #@                  @##K(t(t(@###RRRR@(                        
           B@                ##RQR((tttsRR#####Q##K@/                     
           @@            ^RRQ#R#QRGR@R#############K##S                   
           @@       3#R#######R##stttttS#######K#K######R                 
           @@6%  RR##########R###(tttttS#######B~R#######@^               
          s@%ttt(t(R######K##B###ttttttO#######R   @R######      KRRRRR@  
          ((S@((st(3#######KBR##(tt(ttt7########( Q#######R       BRRRR   
          K(t(R(tSBRR######/ #KRttt(ttt(########@R#######QS     BRs  @    
          K((RKG R#BB####B  /#R(ttt(tt((R######RGS######KB     @#C        
           @@    (BRB##R/   @Qttttttt(ttR#####R(7t(R####K     (##         
           @@      #B#     %KOt7((((((((7#K#KQ(t(% B@###/     @R          
           @@              RR7t((t(((tt((#####(7(G((@B#%      @R          
           @K             RRRRBR(tt7(7C@RR####((#KRK%t        @R          
           @B             @RRRBR@(SRRRRBRB###KO(s(((@         RR(         
           @@           /@RBBRRRBBRBRRBRBB#####t(OSs          (RS         
           K@@        /BRRRRRBRRBRRRRBBRRB##Q#####K^          ^R@         
           /@@       @RRRRRRRBRRBBRBBBBRBB########QO           R@         
            @@       QBRRRRRRRRRRBBBBBRBRBR#######Q#           BB         
            @O      RRRRRRRRRBRBBR@BRBBBBRB########QR          RR         
            @t      OBRBRRRRRBBRRRBBRRBRBRB##########          RR         
            @^      QRRBRRRRRBRBRS @BBBRBRB##########K         BR         
           %@       (@RBRBRRRBR@    RRBRRBB###########        /R@         
           #@         RRRRRRR@      #RRRRBR##########QB       #R@         
           @@         @BBRRRR(       % @QRB###########KB     GB#/         
           s@          #@RRRR@            %@##########R     (RR@          
           %@             @RRRRB@(         OK######KRBRR3/(RRR@           
           @@               @RRRBR         /K###RBRR@ O@RRR@(             
          ^@@              3BR##@              RRRR@^                     
          @@@             /RR#Rs              /RRRR                       
          @@s            /R#RR                @R#R3                       
          @@%           BB#RB                 RRR@                        
          @@K         RRRRRC                 BRRR                         
          @@B       O@R@BRC                 @RRR@                         
          @@@      @@@@@@R                 QRRRR/                         
          %@%     #@@@@@^                s@@@@@@                          
                                        B@@@@@@                           
                                         %(                               
'''

)

# Common way to save a model is to serialize the internal state dictionary.

torch.save(model.state_dict(), "model.pth")
print ("PyTorch model state saved to model.pth")

# Loading model includes re-creating model structure and loading the state
# dictionary into it.

model = NeuralNetwork()
model.load_state_dict(torch.load("model.pth"))

# model can be now used to make predictions.

classes = [
    "T-shirt/top",
    "Trouser",
    "Pullover",
    "Dress",
    "Coat",
    "Sandal",
    "Shirt",
    "Sneaker",
    "Bag",
    "Ankle boot",
]

model.eval()

for i in range(40):

    x, y = test_data[i][0], test_data[i][1]
    with torch.no_grad():   # no_grad stops tracking computational history.
        pred = model(x)
        predicted, actual = classes[pred[0].argmax(0)], classes[y]
        print(f'Predicted: "{predicted}", Actual: "{actual}"')